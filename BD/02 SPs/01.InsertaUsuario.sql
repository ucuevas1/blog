DELIMITER $$
CREATE PROCEDURE `InsertaUsuario`(
	 IN nombre VARCHAR(45)
	,IN apellidos VARCHAR(45)
    ,IN correo VARCHAR(100)
    ,IN contrasenia VARCHAR(50)
    ,IN celular VARCHAR(10)
    ,IN alias VARCHAR(45)
    ,IN tipoUsuario SMALLINT
    ,IN facebook VARCHAR(150)
    ,IN twitter VARCHAR(150)
    ,IN instagram VARCHAR(150)
    ,IN youtube VARCHAR(150)
    )
BEGIN
	/*DECLARACIÓN DE VARIABLES*/
    SET @reg_count = 0;
    SET @fecha_actual = NOW();

	/*VALIDAMOS QUE NO EXISTA UN USUARIO CON ESE CORREO*/
    SELECT 		COUNT(0) 
				INTO @reg_count
		FROM 	usuarios u 
		WHERE 	u.correo 	=	correo;
	
    /*SI EXISTE UN USUARIO MANDAMOS EL MENSAJE DE EXCEPCION*/
    IF @reg_count > 0 THEN
		SIGNAL SQLSTATE '45000' 
        SET message_text = 'USUARIO_CORREO_EXISTE';
    END IF;
    
	/*VALIDAMOS QUE NO EXISTA UN USUARIO CON ESE CELULAR*/
    SELECT		COUNT(0) 
				INTO @reg_count
		FROM	usuarios u 
		WHERE 	u.celular	=	celular;
	
    /*SI EXISTE UN USUARIO MANDAMOS EL MENSAJE DE EXCEPCION*/
    IF @reg_count > 0 THEN
		SIGNAL SQLSTATE '45000' 
        SET message_text = 'USUARIO_CELULAR_EXISTE';
    END IF;
    
	/*SI YA PASARON LAS VALIDACIONES SE INSERTA EL NUEVO REGISTRO EN LA TABA*/
    INSERT INTO usuarios
    (
		nombre,
		apellidos,
		correo,
        contrasenia,
		celular,
		alias,
		fechaalta,
		tipousuario,
		facebook,
		twitter,
		instagram,
        youtube,
        estatus
	)
    VALUES
    (
		nombre,
		apellidos,
		correo,
        contrasenia,
		celular,
		alias,
		@fecha_actual,
		tipousuario,
		facebook,
		twitter,
		instagram,
        youtube,
        1
    );
    
END$$
DELIMITER ;
