DELIMITER $$
CREATE PROCEDURE `LoginUsuario`(
	 IN correo VARCHAR(100)
    ,IN contrasenia VARCHAR(50)
)
BEGIN

	/*PARA POER ACTUALIZAR SIN PRIMARY KEY*/
    SET SQL_SAFE_UPDATES = 0;

	/*DECLARACIÓN DE VARIABLES*/
	SET @token = uuid();
	SET @reg_count = 0;
    
    /*BUSCAMOS EL USUARIO EN LA BD*/
	SELECT		COUNT(0)
				INTO @reg_count
		FROM 	usuarios u
		WHERE 	u.correo 		= 	correo
				AND
				u.contrasenia	=	contrasenia
                AND
                u.estatus		=	1;
	
    /*SI NO SE ENCUENTRAN EL USUARIO CON LOS DATOS INGRESADOS RESPONDEMOS LA EXCEPCIÓN*/
    IF @reg_count = 0 THEN
		SIGNAL SQLSTATE '45000' 
        SET message_text = 'CREDENCIALES_NO_VALIDAS';
    END IF;
    
    /*SI SÍ SE ECUENTRA EL USER SE LE ACTUALIZA EL TOKEN*/
    UPDATE 		usuarios u
		SET		u.token 		= 	@token
        WHERE	u.contrasenia 	=	contrasenia
				AND
                u.correo		= 	correo
                AND
                u.estatus		=	1;

	/*FINALMENTE SELECCIONAMOS EL REGISTRO*/
	SELECT		 u.id
				,u.nombre
                ,u.apellidos
                ,u.correo
                ,u.celular
                ,u.alias
                ,u.fechaalta
                ,u.tipousuario
                ,u.facebook
                ,u.twitter
                ,u.instagram
                ,u.youtube
                ,u.token
		FROM 	usuarios u
		WHERE 	u.correo 		= 	correo
				AND
				u.contrasenia	=	contrasenia
                AND
                u.estatus		=	1;
END$$
DELIMITER ;
