DELIMITER $$
CREATE PROCEDURE `CambiarPublicacionDestacada`(IN id INT)
BEGIN
	UPDATE		publicaciones p
		SET		p.destacado 	=	CASE
										WHEN p.destacado = 1
										THEN 0
										WHEN p.destacado = 0
										THEN 1
									END
		WHERE	p.id			=	id;
END$$
DELIMITER ;
