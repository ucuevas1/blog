DELIMITER $$
CREATE PROCEDURE `InsertaComentario`(
	 IN idpublicacion INT
    ,IN idusuario INT
    ,IN comentario VARCHAR(400)
)
BEGIN
	/*DECLARACION DE VARIABLES*/
    SET @reg_count = 0;
    SET @fecha = NOW();
    
    /*VALIDAMOS QUE EXISTA LA PUBLICACIÓN*/
    SET @reg_count =	(SELECT		COUNT(0)
							FROM	publicaciones p
                            WHERE	p.id	=	idpublicacion);
	
    /*SI NO SE ENCUENTA LA PUBLICACIÓN ENVIAMOS EL MENSAJE*/
    IF @reg_count = 0 THEN
		SIGNAL SQLSTATE '45000' 
        SET message_text = 'PUBLICACION_NO_EXISTE';
    END IF;
    
    /*VALIDAMOS QUE EL USUARIO EXISTA*/
    SET @reg_count =	(SELECT		COUNT(0)
							FROM	usuarios u
                            WHERE	u.id	=	idusuario);
                            
	/*SI NO SE ENCUENTRA EL USUARIO ENVIAMOS LA EXCEPCIÓN*/
    IF @reg_count = 0 THEN
		SIGNAL SQLSTATE '45000' 
        SET message_text = 'USUARIO_NO_EXISTE';
    END IF;
    
    /*SI YA PASÓ LA VALIDACIÓN SE INSERTA EL REGISTRO*/
    INSERT INTO comentarios
    (
				 idpublicacion
                ,idusuario
				,comentario
                ,fecha
    )
    VALUES
    (
				 idpublicacion
                ,idusuario
				,comentario
                ,@fecha
    );
    
END$$
DELIMITER ;
