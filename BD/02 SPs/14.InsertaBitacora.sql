DELIMITER $$
CREATE PROCEDURE `InsertaBitacora`(IN idusuario INT, IN idpublicacion INT, IN accion VARCHAR(300))
BEGIN
	/*Obtenemos la fecha actual*/
	SET @fecha = NOW();
    
    /*Insertamos el registro en la tabla*/
	INSERT INTO bitacora
    (
				 idusuario
				,idpublicacion
                ,accion
                ,fecha
    )
    VALUES
    (
				 idusuario
				,idpublicacion
                ,accion
                ,@fecha
	);
    
END$$
DELIMITER ;
