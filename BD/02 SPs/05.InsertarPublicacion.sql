DELIMITER $$
CREATE PROCEDURE `InsertaPublicacion`(
	 titulo VARCHAR(45)
    ,contenido VARCHAR(10000)
	,idusuario INT
    ,imgbanner VARCHAR(150)
)
BEGIN
	/*DECLARACION DE VARIABLES*/
    SET @reg_count = 0;
    SET @fecha_actual = NOW();
    SET @tipo_usuario = 0;
    
    /*VALIDAMOS QUE EL USUARIO EXISTA*/
    SELECT 		COUNT(0)
				INTO @reg_count	
		FROM 	usuarios u
		WHERE 	u.id 	=	idusuario;
    
	IF @reg_usuario = 0 THEN
		SIGNAL SQLSTATE '45000' 
        SET message_text = 'USUARIO_NO_EXISTE';
    END IF;
    
	/*VALIDAMOS QUE EL USUARIO PUEDA PUBLICAR*/
    SELECT		u.tipousuario 
				INTO @tipo_usuario
		FROM	usuarios u
        WHERE	u.id 	=	idusuario;
	
    IF @tipo_usuario < 1 OR @tipo_usuario > 2 THEN
		SIGNAL SQLSTATE '45000' 
        SET message_text = 'USUARIO_NO_PUBLICA';
    END IF;
	
    /*SI YA PASARON TODAS LAS PUBLICACIONES SE INSERTA EL REGISTRO DE LA PUBLICACIÓN EN ESTATUS DE CREADA*/
    INSERT INTO publicaciones
    (
		 titulo
        ,contenido 
		,fecha 
		,idusuario 
		,imgbanner 
		,destacado 
		,estatus
    )
    VALUES
    (
		 titulo
        ,contenido 
		,@fecha_actual
		,idusuario 
		,imgbanner 
		,0 /*Publicación no destacada*/
		,1 /*estatus creado pero no publicado*/
    );
    
    /*SELECCIONAMOS EL ID DEL REGISTRO RECIEN INSERTADO*/
    SELECT 		MAX(p.id) AS id
		FROM	publicaciones p;
    
END$$
DELIMITER ;
