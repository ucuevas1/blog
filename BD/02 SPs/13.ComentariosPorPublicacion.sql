DELIMITER $$
CREATE PROCEDURE `ComentariosPorPublicacion`(IN idpublicacion INT)
BEGIN
	SELECT		 c.id
				,c.idusuario
                ,c.comentario
                ,DATE_FORMAT(c.fecha,"%d/%m/%Y %H:%i" ) as fecha
                ,u.alias
		FROM	comentarios c
				INNER JOIN usuarios u
					ON u.id			=	c.idusuario
        WHERE	c.idpublicacion		=	idpublicacion
        ORDER BY
				c.fecha;
				
END$$
DELIMITER ;
