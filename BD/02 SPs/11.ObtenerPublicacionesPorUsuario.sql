DELIMITER $$
CREATE PROCEDURE `ObtenerPublicacionesPorUsuario`(IN idusuario INT)
BEGIN
	/*INICIALIZACIÓN DE VARIABLES*/
	SET @tipo = 0;
    
    /*OBTENEMOS EL TIPO DE USUARIO EN LA VARIABLE*/
    SELECT			u.tipousuario
					INTO @tipo
		FROM		usuarios u
		WHERE		u.id		=	idusuario;

	/**/
	SELECT			 p.id
					,p.titulo
					,p.contenido
					,DATE_FORMAT(p.fecha,"%d/%m/%Y %H:%i") AS fecha 
                    ,p.imgbanner
                    ,CASE
						WHEN p.destacado = 1
                        THEN "Si"
                        WHEN p.destacado = 0
                        THEN "No"
					END AS destacado
                    ,CASE 
						WHEN p.estatus = 1
                        THEN "Creada"
                        WHEN p.estatus = 2
                        THEN "Publicada"
                        WHEN p.estatus = 3
                        THEN "Bajada"
					END AS estatus
                    ,u.nombre
                    ,u.apellidos
                    ,u.alias
                    ,u.correo
                    ,CASE
						WHEN u.tipousuario = 1
                        THEN "Administrador"
                        ELSE "Generador de contenido"
					END AS tipo
		FROM		publicaciones p
					INNER JOIN usuarios u
						ON	p.idusuario		=	u.id
        WHERE		p.idusuario				=	CASE 
													WHEN @tipo = 1 
													THEN p.idusuario
													ELSE idusuario
												END
		ORDER BY	p.fecha
        DESC;
                                            
END$$
DELIMITER ;
