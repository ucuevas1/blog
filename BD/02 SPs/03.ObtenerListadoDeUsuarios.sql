DELIMITER $$
CREATE PROCEDURE `ObtenerListadoDeUsuarios`()
BEGIN
	SELECT			 u.id
					,u.nombre
                    ,u.apellidos
                    ,u.correo
                    ,u.celular
                    ,u.alias
                    ,DATE_FORMAT(u.fechaalta,"%d/%m/%Y %H:%i") AS fecha
                    ,CASE
						WHEN u.tipousuario = 1
                        THEN "Administrador"
                        WHEN u.tipousuario = 2
                        THEN "Generador de contenido"
                        WHEN u.tipousuario = 3
                        THEN "Visitante"
					END AS tipousuario
                    ,CASE
						WHEN u.estatus	= 1
                        THEN "Activo"
                        WHEN u.estatus	= 0
                        THEN "Desactivado"
					END AS estatus
		FROM		usuarios u;
			
END$$
DELIMITER ;
