DELIMITER $$
CREATE PROCEDURE `AgregaSeccion`(
	 idpublicacion	INT
    ,encabezado	VARCHAR(45)
    ,contenido VARCHAR(5000)
)
BEGIN

	/*DECLARACION VARIABLES*/
    SET @reg_count = 0;

	/*OBTENEMOS LA CANTIDAD DE SECCIONES INGRESADAS EN LA PUBLICACIÓN*/
	SELECT		COUNT(0)
				INTO @reg_count
		FROM	secciones_publicacion s 
		WHERE	s.idpublicacion = idpublicacion;

	/*INSERTAMOS EL REGISTRO EN LA TABLA*/
    INSERT INTO secciones_publicacion
    (
		 idpublicacion
        ,orden
        ,encabezado
        ,contenido
    )
    VALUES
    (
		 idpublicacion
        ,@reg_count + 1
        ,encabezado
        ,contenido
    );

END$$
DELIMITER ;
