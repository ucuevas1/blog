DELIMITER $$
CREATE PROCEDURE `CambiarEstatusUsuario`(IN id INT)
BEGIN

	UPDATE		usuarios u
		SET		u.estatus = 	CASE
									WHEN u.estatus = 1
									THEN 0
									WHEN u.estatus = 0
									THEN 1
								END
		WHERE	u.id		=	id;

END$$
DELIMITER ;
