DELIMITER $$
CREATE PROCEDURE `CambiarEstatusPublicacion`(IN id INT)
BEGIN
	UPDATE		publicaciones u
		SET		u.estatus	=	CASE
									WHEN u.estatus = 1
                                    THEN 2
									WHEN u.estatus = 2
                                    THEN 3
                                    WHEN u.estatus = 3
                                    THEN 2
								END
		WHERE	u.id		=	id;
END$$
DELIMITER ;
