DELIMITER $$
CREATE PROCEDURE `ObtenerPublicaciones`(IN destacado TINYINT )
BEGIN

	SELECT		 p.id
				,p.titulo
                ,p.contenido
                ,DATE_FORMAT(p.fecha,"%d/%m/%Y %H:%i" ) as fecha
                ,p.imgbanner
                ,u.alias
                ,CASE 
					WHEN u.tipousuario = 1
					THEN "Administrador del sitio"
					ELSE "Creador de contenido"
				 END AS tipousuario
				,u.facebook
                ,u.twitter
                ,u.instagram
                ,u.youtube
		FROM 	publicaciones p
				INNER JOIN usuarios u
					ON u.id 	= 	p.idusuario
		WHERE	p.destacado 	= 	destacado
				AND
                p.estatus		=	2
		ORDER BY
				p.fecha
		DESC;
                
END$$
DELIMITER ;
