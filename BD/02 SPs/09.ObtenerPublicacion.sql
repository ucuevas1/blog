DELIMITER $$
CREATE PROCEDURE `ObtenerPublicacion`(IN id INT)
BEGIN
	SELECT		 p.titulo
				,p.contenido
                ,p.imgbanner
		FROM 	publicaciones p
        WHERE	p.id = id
	UNION
    (SELECT		 s.encabezado
				,s.contenido
                ,''
		FROM 	secciones_publicacion s
        WHERE	s.idpublicacion = id
        ORDER BY
				s.orden);
                
	SELECT		 u.id
				,u.nombre
                ,u.apellidos
                ,u.correo
                ,u.celular
                ,u.alias
                ,CASE 
					WHEN u.tipousuario = 1
                    THEN "Administrador del sitio"
                    ELSE "Creador de contenido"
				 END AS tipousuario
                ,u.facebook
                ,u.twitter
                ,u.instagram
                ,u.youtube
		FROM	usuarios u
				INNER JOIN publicaciones p
					ON p.idusuario	=	u.id
		WHERE	p.id 				=	id;
		
END$$
DELIMITER ;
