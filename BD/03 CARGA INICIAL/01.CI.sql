/*INSERCIÓN DE REGISTRO DE USUARIO ADMINISTRADOR, PUEDEN PONER LOS DATOS QUE DESEEN
TOMANDO EN CUENTA QUE PARA INGRESAR SE NECESITA EL CORREO Y LA CONTRASEÑA, EN ESTE CASO
LA CONTRASEÑA NO ESTÁ CIFRADA*/
INSERT INTO usuarios
(
			 nombre
			,apellidos
            ,correo
            ,contrasenia
            ,celular
            ,alias
            ,fechaalta
            ,tipousuario
            ,facebook
            ,twitter
            ,instagram
            ,youtube
            ,token
            ,estatus
)
VALUES
(
			 "Marcos Ulises"
			,"Cuevas Castro"
            ,"ucuevas.castro@hotmail.com"
            ,"password"
            ,"8113770783"
            ,"Ulises"
            ,NOW() /*FECHA ACTUAL*/
            ,1 /*USUARIO ADMINISTRADOR*/
            ,"" /*FACEBOOK*/
            ,"" /*TWITTER*/
            ,"" /*INSTAGRAM*/
            ,"" /*YOUTUBE*/
            ,"" /*TOKEN*/
            ,1 /*ESTATUS ACTIVO*/
)