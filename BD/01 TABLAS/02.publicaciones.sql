CREATE TABLE `publicaciones` (
  `id` int NOT NULL AUTO_INCREMENT,
  `titulo` varchar(45) NOT NULL,
  `contenido` varchar(10000) NOT NULL,
  `fecha` datetime NOT NULL,
  `idusuario` int NOT NULL,
  `imgbanner` varchar(150) NOT NULL,
  `destacado` tinyint NOT NULL,
  `estatus` smallint NOT NULL,
  PRIMARY KEY (`id`),
  KEY `usuariopublica_idx` (`idusuario`),
  CONSTRAINT `usuariopublica` FOREIGN KEY (`idusuario`) REFERENCES `usuarios` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='Tabla en la que se almacenará el contenido de las publicaciones en el blog';
