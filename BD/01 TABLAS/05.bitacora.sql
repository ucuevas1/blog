CREATE TABLE `bitacora` (
  `id` int NOT NULL AUTO_INCREMENT,
  `idusuario` int DEFAULT NULL,
  `idpublicacion` int DEFAULT NULL,
  `accion` varchar(300) NOT NULL,
  `fecha` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
