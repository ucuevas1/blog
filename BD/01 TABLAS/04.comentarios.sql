CREATE TABLE `comentarios` (
  `id` int NOT NULL AUTO_INCREMENT,
  `idpublicacion` int NOT NULL,
  `idusuario` int NOT NULL,
  `comentario` varchar(400) NOT NULL,
  `fecha` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `usuariocomenta_idx` (`idusuario`),
  KEY `publicacioncomentario_idx` (`idpublicacion`),
  CONSTRAINT `publicacioncomentario` FOREIGN KEY (`idpublicacion`) REFERENCES `publicaciones` (`id`),
  CONSTRAINT `usuariocomenta` FOREIGN KEY (`idusuario`) REFERENCES `usuarios` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
