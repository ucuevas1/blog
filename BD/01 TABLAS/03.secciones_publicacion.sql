CREATE TABLE `secciones_publicacion` (
  `id` int NOT NULL AUTO_INCREMENT,
  `idpublicacion` int NOT NULL,
  `orden` int NOT NULL,
  `encabezado` varchar(45) NOT NULL,
  `contenido` varchar(5000) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `publicacionseccion_idx` (`idpublicacion`),
  CONSTRAINT `publicacionseccion` FOREIGN KEY (`idpublicacion`) REFERENCES `publicaciones` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='Contenido de secciones de la publicación';
