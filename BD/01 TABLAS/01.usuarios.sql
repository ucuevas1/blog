CREATE TABLE `usuarios` (
  `id` int NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) NOT NULL,
  `apellidos` varchar(45) NOT NULL,
  `correo` varchar(100) NOT NULL,
  `contrasenia` varchar(50) DEFAULT NULL,
  `celular` varchar(10) NOT NULL,
  `alias` varchar(45) NOT NULL,
  `fechaalta` datetime NOT NULL,
  `tipousuario` smallint NOT NULL,
  `facebook` varchar(150) NOT NULL,
  `twitter` varchar(150) NOT NULL,
  `instagram` varchar(150) NOT NULL,
  `youtube` varchar(150) NOT NULL,
  `token` varchar(50) DEFAULT NULL,
  `estatus` tinyint DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='Registro de usuarios del blog';
