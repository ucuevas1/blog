const regExpEmail = /.+@.+\..+/;
const regExpNumbers = /^\d+$/;

exports.validateNewUser = function(body){
    if (body.nombre == undefined || body.nombre == null || body.nombre == "" ){
        return "Se debe proporcionar un nombre";
    }
    else if (body.apellidos == undefined || body.apellidos == null || body.apellidos == "" ){
        return "Se deben proporcionar los apellidos";
    }
    else if (body.correo == undefined || body.correo == null || body.correo == "" ){
        return "Se deben proporcionar el correo del usuario";
    }
    else if (body.correo.match(regExpEmail) == null){
        return "Se deben proporcionar un correo válido";
    }
    else if (body.contrasenia == undefined || body.contrasenia == null || body.contrasenia == ""){
        return "Se deben proporcionar la contraseña";
    }
    else if (body.celular == undefined || body.celular == null || body.celular == "" ){
        return "Se debe proporcionar un celular";
    }
    else if (body.celular.length != 10 || body.celular.match(regExpNumbers) == null ){
        return "Se debe proporcionar un celular válido";
    }
    else if (body.alias == undefined || body.alias == null || body.alias == "" ){
        return "Se debe proporcionar el alias del usuario";
    }
    else if (body.tipo == undefined || body.tipo == null || body.tipo == ""){
        return "Se debe proporcionar el tipo de usuario";
    }
    else{
        return "";
    }
}

exports.validateLogin = function(body){
    if (body.correo == undefined || body.correo == null || body.correo == ""){
        return "Se debe ingresar un correo";
    }
    else if (body.correo.match(regExpEmail) == null){
        return "Se debe ingresar un correo válido";
    }
    else if(body.contrasenia == undefined || body.contrasenia == null || body.contrasenia == ""){
        return "Se debe de ingresar la contraseña";
    }
    else{
        return "";
    }
}

exports.messageBySqlException = function(sqlExMessage){
    switch(sqlExMessage){
        case "USUARIO_CORREO_EXISTE":
            return "Ya existe un usuario con ese correo";
        case "USUARIO_CELULAR_EXISTE":
            return "Ya existe un usuario con ese celular";
        case "USUARIO_NO_EXISTE":
            return "El usuario ingresado no existe";
        case "USUARIO_NO_PUBLICA":
            return "El usuario no puede realizar publicaciones";
        case "USUARIO_NO_TEMAS":
            return "El usuario no puede registrar temas";
        case "CREDENCIALES_NO_VALIDAS":
            return "Las credenciales ingresadas no son válidas";
        case "PUBLICACION_NO_EXISTE":
            return "La publicación ingresada no existe";
        default:
            return "No se pudo realizar el proceso, intenta más tarde";
    }
}

exports.validateGetPosts = function(query){
    var pinnedUp = query.pinnedUp;
    if (pinnedUp == undefined || pinnedUp == null || pinnedUp == "" ){
        return "Se necesita el parámetro pinnedUp";
    }
    else if(pinnedUp.toString() != "1" && pinnedUp.toString() != "0"){
        return "El parámetro pinnedUp no es válido";
    }
    else{
        return "";
    }
}