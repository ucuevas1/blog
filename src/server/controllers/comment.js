const url = require("url");
const validator = require("../helpers/validator.js");
const blogDAO = require("../dao/blogDAO.js")
const config = require("../../config.json")
const fs = require("fs");
const path = require('path');

module.exports = {
    InsertComment : InsertComment
}

function InsertComment(req,res){
    console.log("InsertComment Request", JSON.stringify(req.body));
    blogDAO.InsertComment(req.body.idpost,req.body.iduser,req.body.comment,function(error,result){
        if(error){
            console.log(error);
            req.session.msg = validator.messageBySqlException(error.sqlMessage);
        }
        blogDAO.insertLog(req.body.iduser,req.body.idpost,"Comentario en publicación")
        return res.redirect("/post/detail?id=" + req.body.idpost);
    });
}