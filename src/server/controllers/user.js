const url = require("url");
const validator = require("../helpers/validator.js");
const blogDAO = require("../dao/blogDAO.js")
const path = require("path");
const fs = require("fs");
/*const opts = {
	logDirectory: path.join(__dirname,config.rutaLogs),
    fileNamePattern:config.nombreLog,
    dateFormat: config.formatoFechaLog
};
const log = require('simple-node-logger').createRollingFileLogger(opts);*/

module.exports  = {
    addUser : addUser,
    loginUser : loginUser,
    getUsersAdmin : getUsersAdmin,
    redirectAddUser : redirectAddUser,
    addUserByAdmin : addUserByAdmin,
    userChangeStatus : userChangeStatus
}

function addUser(req, res){
    res.setHeader('Content-Type', 'application/json');
    console.log("addUser - req - ", JSON.stringify(req.body));
    var resultValidator = validator.validateNewUser(req.body)
    if (resultValidator != ""){
        blogDAO.insertLog(0,0,"Usuario con correo " + req.body.correo + " y celular" + req.body.correo + " se registró sin éxito, mensaje " + resultValidator) 
        req.session.regResp = {msg : resultValidator};
        return res.redirect("/login?opt=2");
        //return res.status(400).json({msg : resultValidator});
    }
    blogDAO.InsertUser(req.body,function(error,result){
        if(error){
            blogDAO.insertLog(0,0,"Usuario con correo " + req.body.correo + " y celular" + req.body.correo + " se registró sin éxito, mensaje " + validator.messageBySqlException(error.sqlMessage)) 
            req.session.regResp = {msg:validator.messageBySqlException(error.sqlMessage)};
            return res.redirect("/login?opt=2");
        }
        blogDAO.insertLog(0,0,"Usuario con correo " + req.body.correo + " y celular" + req.body.correo + " se registró con éxito" ) 
        // return res.send(jsonResp);
        req.session.regResp = {msg :"OK"}
        return res.redirect("/login?opt=1");
    });
}

function loginUser(req,res){
    var resultValidator = validator.validateLogin(req.body);
    if(resultValidator != ""){
        blogDAO.insertLog(0,0,"Usuario con correo " + req.body.correo + " intentó ingresar sin éxito. Mensaje: " + resultValidator);
        req.session.jsonResp = {msg : resultValidator};
        return res.redirect("/login?opt=1")
        
    }
    blogDAO.LoginUser(req.body,function(error,result){
        if(error){
            blogDAO.insertLog(0,0,"Usuario con correo " + req.correo + " intentó ingresar sin éxito. Mensaje: " + validator.messageBySqlException(error.sqlMessage));
            req.session.jsonResp = {msg : validator.messageBySqlException(error.sqlMessage)};
            return res.redirect("/login?opt=1")
            //return res.status(400).json({msg : validator.messageBySqlException(error.sqlMessage)});
        }
        /*var jsonResp = {
            msg : "OK"
            usuario : result[0][0]
        }*/
        blogDAO.insertLog(result[0][0].id,0,"Inicio de sesión exitosa");
        req.session.usuario = result[0][0];
        return res.redirect("/");
    });

}

function getUsersAdmin(req,res){
    blogDAO.getUsersList(function(error,result){
        if(error){
            console.log(JSON.stringify(error));
            res.redirect("/")
        }

        var rows = "";
        for (i=0;i<=result[0].length-1;i++){
            rows = rows + getUserRow(result[0][i],req.session.usuario);
        }

        var html = fs.readFileSync(path.join(__dirname, '../../client/views/') + 'adminusers.html', 'utf8');
        var linkAdminPosts = "";
        if(req.session.usuario.tipousuario == 1){
            redirAdminUsers = "<a class='post_detail' href='/post/admin'>Administrar publicaciones</a>"
        }
        html = html.replace("{adminuserssection}",rows).replace("{sesion-login}",redirAdminUsers + " / <a class='post_detail' href='/logout'>Cerrar Sesión</a>");
        return res.send(html)

    });
}

function redirectAddUser(req,res){
    var msg = ""
    if(req.session.msg){
        msg = req.session.msg
    }
    var html = fs.readFileSync(path.join(__dirname, '../../client/views/') + 'registroadmin.html', 'utf8'); 
    html = html.replace("{msg}",msg);
    return res.send(html);
}

function addUserByAdmin(req,res){
    var resultValidator = validator.validateNewUser(req.body)
    if (resultValidator != ""){
        blogDAO.insertLog(req.session.usuario.id,0,"Intento de registrar usuario sin éxito. Mensaje: " + resultValidator);
        req.session.msg = resultValidator;
        return res.redirect("/user/add");
    }
    blogDAO.InsertUser(req.body,function(error,result){
        if(error){
            blogDAO.insertLog(req.session.usuario.id,0,"Intento de registrar usuario sin éxito. Mensaje: " +  validator.messageBySqlException(error.sqlMessage));
            req.session.msg = validator.messageBySqlException(error.sqlMessage);
            return res.redirect("/user/add");
        }
        blogDAO.insertLog(req.session.usuario.id,0,"Registro de usuario exitoso con correo " + req.body.correo + " y celular " + req.body.celular + " por usuario administrador");
        return res.redirect("/user/admin");
    });
    
}

function userChangeStatus(req,res){
    var queryString = url.parse(req.url,true).query;
    console.log(queryString);
    blogDAO.updateUserStatus(queryString.id,function(error,result){
        if(error){
            console.log(JSON.stringify(error));
        }
        blogDAO.insertLog(req.session.usuario.id,0,"Cambio de estatus a usuario con identificador " + queryString.id.toString());
        return res.redirect("/user/admin");
    });
}

/**MÉTODOS LOCALES**/
function getUserRow(user,loggedUser){
    var row = '<tr>' +
                '<td class="bordercell">' +
                    user.nombre +
                '</td>' +
                '<td class="bordercell">' +
                    user.apellidos +
                '</td>' +
                '<td class="bordercell">' +
                    user.alias +
                '</td>' +
                '<td class="bordercell">' +
                    user.correo +
                '</td>' +
                '<td class="bordercell">' +
                    user.celular +
                '</td>' +
                '<td class="bordercell">' +
                    user.fecha +
                '</td>' +
                '<td class="bordercell">' +
                   user.tipousuario + 
                '</td>' +
                '<td class="bordercell">' +
                   user.estatus + 
                '</td>'

                if(loggedUser.id.toString() == user.id.toString()){
                    row = row + '<td class="bordercell"></td></tr>'
                }
                else{
                    row = row + '<td class="bordercell">' +
                                    '<a href="/user/status?id=' + user.id.toString() + '">Cambiar Estatus</a>' + 
                                '</td></tr>'
                }
    return row;
}