const url = require("url");
const path = require('path');
const fs = require("fs");
const blogDAO = require("../dao/blogDAO");

module.exports = {
    getLoginRedirect : getLoginRedirect,
    logout : logout
}

function getLoginRedirect(req,res){
    var query = url.parse(req.url,true).query;
    console.log("session", JSON.stringify(req.session));

    var msg = "";
    if (req.session.jsonResp){
        if (req.session.jsonResp.msg == "OK"){
            usuario = req.session.jsonResp.usuario;
            req.session.destroy(function(err){
                req.session.usuario = usuario;
                return res.redirect("/");
            });
        }
        else{
            msg = req.session.jsonResp.msg;
            req.session.destroy(function(err){});
        } 
    }

    if (query.opt == 2){
        if(req.session.regResp){
            if(req.session.regResp.msg != "OK"){
                msg = req.session.regResp.msg;
            }
        }
    }
    
    var html = "";
    if(query.opt == 1){
        html = fs.readFileSync(path.join(__dirname, '../../client/views/') + 'login.html', 'utf8');
        html = html.replace("{msg}",msg);
        res.send(html);
    }
    else if (query.opt == 2){
        html = fs.readFileSync(path.join(__dirname, '../../client/views/') + 'registro.html', 'utf8');
        html = html.replace("{msg}",msg);
        res.send(html);
    }
    else{
        res.redirect("/");
    }
}

function logout(req,res){
    blogDAO.insertLog(req.session.usuario.id,0,"Usuario sale del sistema.")
    req.session.destroy(function(err){
        res.redirect("/");
    });
}