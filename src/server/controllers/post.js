const url = require("url");
const validator = require("../helpers/validator.js");
const blogDAO = require("../dao/blogDAO.js")
const fs = require("fs");
const path = require('path');

const { DH_NOT_SUITABLE_GENERATOR } = require("constants");
/*const opts = {
	logDirectory: path.join(__dirname,config.rutaLogs),
    fileNamePattern:config.nombreLog,
    dateFormat: config.formatoFechaLog
};
const log = require('simple-node-logger').createRollingFileLogger(opts);*/

/**EXPOSICIÓN DE MÉTODOS**/
module.exports = {
    getPosts : getPosts,
    getLanding : getLanding,
    getPost : getPost,
    getAdminPosts : getAdminPosts,
    updateStatus : updateStatus,
    updateHighlight : updateHighlight,
    redirectNewPost : redirectNewPost,
    addNewPost : addNewPost,
    postUpload : postUpload,
    redirectUploadTest : redirectUploadTest,
}

/**MÉTODOS EXPUESTOS**/
function getPosts(req, res){
    res.setHeader('Content-Type', 'application/json');
    console.log("getPost - req - ", JSON.stringify(url.parse(req.url,true).query));
    var resultValidate = validator.validateGetPosts(url.parse(req.url,true).query);
    
    if (resultValidate != ""){
        console.log("getPosts - validator - ", resultValidate)
        return res.status(400).json({msg : resultValidate});
    }

    var pinnedUp = url.parse(req.url,true).query.pinnedUp;
    blogDAO.getPosts(pinnedUp,function(error,result){
        if (error){
            console.log("getPosts - selectPosts - ", JSON.stringify(error));
            return res.status(400).json({msg : validator.messageBySqlException(error.sqlMessage)});
        }
        var jsonResp = {
            msg : "OK",
            posts : result[0]
        }
        console.log("getPosts - res - ", JSON.stringify(jsonResp));
        return res.send(jsonResp);
    });

}

function getLanding(req, res){
    blogDAO.getPosts(1,function(error,result){
        if (error){
            console.log("getLanding - selectPosts - ", JSON.stringify(error));
            return res.status(400).json({msg : validator.messageBySqlException(error.sqlMessage)});
        }
        
        blogDAO.getPosts(0,function(error,result2){
            if (error){
                console.log("getLanding - selectPosts2 - ", JSON.stringify(error));
                return res.status(400).json({msg : validator.messageBySqlException(error.sqlMessage)});
            }

            console.log("getLanding - selectCountPinnedUd - ", result[0].length.toString());
            var cells = ""
            for (i=0;i<=result[0].length-1;i++){
                cells = cells + getCellFromPost(result[0][i]);
            }

            console.log("getLanding - selectCountNotPinnedUd - ", result2[0].length.toString());
            var rows = ""
            for (i=0;i<=result2[0].length-1;i++){
                rows = rows + getRowsFromPost(result2[0][i]);
            }   

            console.log("getLanding - redirecting to landing... ");
            var html = fs.readFileSync(path.join(__dirname, '../../client/views/') + 'landing.html', 'utf8');
            html = html.replace("{cells}",cells).replace("{rows}",rows);

            if(req.session.usuario){
                console.log(JSON.stringify(req.session.usuario))
                var linkAdminPosts = ""
                if (req.session.usuario.tipousuario == 1 || req.session.usuario.tipousuario == 2){
                    linkAdminPosts = " / <a class='post_detail' href='/post/admin'>Administrar publicaciones</a>"
                    if(req.session.usuario.tipousuario == 1){
                        linkAdminPosts = linkAdminPosts + " / <a class='post_detail' href='/user/admin'>Administrar usuarios</a>"
                    }
                }
                html = html.replace("{sesion-login}","<t class='post_detail' >Hola " + req.session.usuario.alias +"</t> " + linkAdminPosts + " / <a class='post_detail' href='/logout'>Cerrar Sesión<a/>")
                return res.send(html);
            }else{
                html = html.replace("{sesion-login}","<a class='post_detail' href='/login?opt=1'>Inicia Sesión<a/> / <a class='post_detail' href='/login?opt=2'>Regístrate<a/>")
                return res.send(html);
            }
        });
    });
}

function getPost(req,res){
    var queryString = url.parse(req.url,true).query;
    blogDAO.getPostDetail(queryString.id,function(error,result){
        if(error){
            console.log("getPost - selectPost - ", JSON.stringify(error));
            return res.status(400).json({msg:validator.messageBySqlException(error.sqlMessage)});
        }
        
        blogDAO.getPostComments(queryString.id,function(error,comments){
            if(error){
                console.log("getPost - selectComments - ", JSON.stringify(error));
                return res.status(400).json({msg:validator.messageBySqlException(error.sqlMessage)});
            }

            var sections = "";
            for (i=1;i<=result[0].length-1;i++){
                sections = sections + getPostDetailSection(result[0][i]);
            }

            var commnetsSection = "";
            for (j=0;j<=comments[0].length-1;j++){
                commnetsSection = commnetsSection + getCommentsSection(comments[0][j]);
            }

            var html = fs.readFileSync(path.join(__dirname, '../../client/views/') + 'postdetail.html', 'utf8');
                html = html.replace("{imgurl}",result[0][0].imgbanner).replace("{titulopost}",result[0][0].titulo)
                        .replace("{contenidopost}",result[0][0].contenido).replace("{detail}",sections).replace("{comments}",commnetsSection);

            if(req.session.usuario){
                var htmlLinks = "";
                if(req.session.usuario.tipousuario == 1 || req.session.usuario.tipousuario == 2){
                    htmlLinks = "<a class='post_detail' href='/post/admin'>Administrar publicaciones</a>";
                    if(req.session.usuario.tipousuario == 1){
                        htmlLinks = htmlLinks + " / <a class='post_detail' href='/user/admin'>Administrar usuarios</a>";
                    }
                }
                html = html.replace("{sesion-login}","<t class='post_detail' >Hola " + req.session.usuario.alias +"</t> " + htmlLinks + " / <a class='post_detail' href='/logout'>Cerrar Sesión<a/>")
                           .replace("{CSvisibility}","").replace("{iduser}",req.session.usuario.id.toString()).replace("{idpost}",queryString.id)
                return res.send(html);
            }else{
                html = html.replace("{sesion-login}","<a class='post_detail' href='/login?opt=1'>Inicia Sesión<a/> / <a class='post_detail' href='/login?opt=2'>Regístrate<a/>")
                           .replace("{CSvisibility}","class='hide'")
                return res.send(html);
            }
        });
    });
}

function getAdminPosts(req,res){
    blogDAO.getPostsByIdUser(req.session.usuario.id,function(error,result){
        if(error){
            console.log(error);
            return res.redirect("/");
        }
        var adminPostSection = "";
        for (i=0;i<=result[0].length-1;i++){
            adminPostSection = adminPostSection + getAdminPostSection(result[0][i]);
        }

        var html = fs.readFileSync(path.join(__dirname, '../../client/views/') + 'adminpost.html', 'utf8');
        var redirAdminUsers = "";
        if(req.session.usuario.tipousuario == 1){
            redirAdminUsers = " / <a class='post_detail' href='/user/admin'>Administrar usuarios</a>";
        }
        
        html = html.replace("{adminpostsection}",adminPostSection).replace("{sesion-login}",redirAdminUsers + " / <a class='post_detail' href='/logout'>Cerrar Sesión<a/>");
        res.send(html);
    });
}

function updateStatus(req,res){
    var queryString = url.parse(req.url,true).query;
    blogDAO.updatePostStatus(queryString.id,function(error,result){
        if(error){
            blogDAO.insertLog(req.session.usuario.id,queryString.id,"Cambio de estatus de publicación sin éxito.")
        }
        blogDAO.insertLog(req.session.usuario.id,queryString.id,"Cambio de estatus de publicación con éxito.")
        return res.redirect("/post/admin")
    });
    
}

function updateHighlight(req,res){
    var queryString = url.parse(req.url,true).query;
    blogDAO.updatePostHighlight(queryString.id,function(error,result){
        if(error){
            blogDAO.insertLog(req.session.usuario.id,queryString.id,"Cambio destacado de publicación sin éxito.")
        }
        blogDAO.insertLog(req.session.usuario.id,queryString.id,"Cambio destacado de publicación con éxito.")
        return res.redirect("/post/admin")
    });
    
}

function redirectUploadTest(req,res){
    return res.sendFile(path.join(__dirname, '../../client/views/') + 'uploadtest.html', 'utf8')
}

function postUpload(req,res){
    console.log(req.body);
    res.redirect("/uploadtest");
}

function redirectNewPost(req,res){
    fs.readdir(path.join(__dirname, '../../client/imgs'), (err, files) => {
        var imgs = "";
        for (i=0;i<=files.length-1;i++){
            imgs = imgs + getImageOptions(files[i]);
        }

        var msg = "";
        if(req.session.msg){
            msg = req.session.msg
        }

        var link = " / <a class='post_detail' href='/post/admin'>Administrar publicaciones</a>";
        if (req.session.usuario.tipousuario == 1){
            link = link + " / <a class='post_detail' href='/user/admin'>Administrar usuarios</a>";
        }

        var html = fs.readFileSync(path.join(__dirname, '../../client/views/') + 'newpost.html', 'utf8')
        html = html.replace("{opciones}",imgs).replace("{msg}",msg).replace("{sesion-login}",link);

        return res.send(html);    
    });
    
}

function addNewPost(req,res){
    var elems = req.body.contenido.split("|");
    if(!isPair(elems.length) || elems.length < 4 ){
        blogDAO.insertLog(req.session.usuario.id,0,"Intento de realizar nueva publicación sin éxito. Mensaje: El formato de entrada no es correcto.")
        req.session.msg = "El formato de entrada no es correcto."
    }
    
    //Insertamos primero el cabecero que corresponde a los primero dos elementos
    blogDAO.insertPost(elems[0],elems[1],req.session.usuario.id,req.body.imgbanner,function(error,result){
        if(error){
            blogDAO.insertLog(req.session.usuario.id,0,"Intento de realizar nueva publicación sin éxito. Mensaje: " + validator.messageBySqlException(error.sqlMessage))
            req.session.msg = validator.messageBySqlException(error.sqlMessage);
            return res.redirect("/post/new");
        }
        //Procedemos a insertar el detalle que son el resto del arreglo
        for (i=2;i<=elems.length-2;i+=2){
            blogDAO.insertPostDetail(result[0][0].id,elems[i],elems[i+1],function(error,resDetail){
                if(error){console.log(error)};
            });
        }
        //Redirecciona a el admin de las publicciones
        blogDAO.insertLog(req.session.usuario.id,result[0][0].id,"Publicación insertada exitosamente.")
        return res.redirect("/post/admin")
    });
}

/**MÉTODOS LOCALES**/
function getCellFromPost(post){

    var cell =  '<td>' +
                '<t class="por"><b>{Titulo}</b></t>' +
                '<br/>' +
                '<img src="../../{ImgURL}" width="250px" />' +
                '<br/>' +
                '<t class="por">Por {Autor}. {Tipo}</t>' +
                '<br/>' +
                '<t class="detalle"><b>Fecha publicación: {fecha}</b></t>' +
                '<br/>' +
                '<t class="detalle">{detalle}</t>' +
                '<a href="/post/detail?id={id}">Ver mas</a>' +
                '</td>'

    return cell.replace('{Titulo}',post.titulo).replace("{ImgURL}",post.imgbanner).replace("{Autor}",post.alias)
               .replace("{Tipo}",post.tipousuario).replace("{detalle}",post.contenido).replace("{id}",post.id)
               .replace("{fecha}",post.fecha);
}

function getRowsFromPost(post){
    var row = "<tr style='vertical-align:top;'>" +
                    "<td style='width:40%;'>" +
                        "<img src='../../" + post.imgbanner + "' width='100%'/>" +
                    "</td>" +
                    "<td style='width:60%;'>" +
                        "<t class='destacados'>" +
                            post.titulo +
                        "</t><br/><br/>" +
                        "<t class='post_detail'>" +
                            post.contenido +
                            "<a href='/post/detail?id=" + post.id.toString() + "'>Ver mas...</a>" +
                        "</t>" +
                        "<br/><br/>" +
                        "<t class='por'>" +
                            "Por " + post.alias + ". " + post.tipousuario  +
                        "</t><br/>" +
                        "<t class='detalle'>" +
                            "<b>Fecha de publicación: " + post.fecha +
                        "</t>" +
                        "<br/>" 
                        
                        if (post.facebook != ""){
                            row = row + "<a href='" + post.facebook + "' target='_blank'>" +
                                            "<img src='../imgs/facebook.jpg' width='30px' />" +
                                        "</a>" 
                        }
                        
                        if (post.twitter != ""){
                            row = row + "<a href='" + post.twitter + "' target='_blank'>" +
                                            "<img src='../imgs/twitter.jpg' width='50px' />" +
                                        "</a>"
                        }
                        
                        if (post.instagram != ""){
                            row = row + "<a href='" + post.instagram + "' target='_blank'>" +
                                            "<img src='../imgs/instagram.jpg' width='35px' />" +
                                        "</a>"
                        }
                        
                        if (post.youtube != ""){
                            row = row + "<a href='" + post.youtube + "' target='_blank'>" +
                                            "<img src='../imgs/youtube.jpg' width='45px' />" +
                                        "</a>"
                        }
                         
                    "</td>" +
                "</tr>"
    return row;
}   

function getPostDetailSection(section){
    var sect = "<t class='destacados'>" +
                section.titulo +
                "</t><br/><br/>" +
                "<t class='post_detail'>" +
                section.contenido + 
                "<br/><br/>" +
                "</t>"
    return sect;   
}

function getCommentsSection(comment){
    var commentSection = "<tr>" +
                         "<td style='vertical-align: top;'>" +
                         "<t class='por'><b>{alias}: </b></t>" +
                         "</td>" +
                         "<td style='width: 400px;vertical-align: top;'>" +
                         "<t class='post_detail'>{comment}</t>" +
                         "<br/>" +
                         "<t class='por'><b>Fecha: </b></t><t class='post_detail'> {fecha}</t><br/>" +
                         "</td>" +
                         "</tr>";

    return commentSection.replace("{alias}",comment.alias).replace("{comment}",comment.comentario).replace("{fecha}",comment.fecha);
}

function getAdminPostSection(post){
    var htmlRet = "<tr>" +
                    "<td class='bordercell'>" +
                        "{titulo}" +
                    "</td>" +
                    "<td class='bordercell'>" +
                        "{contenido}" +
                    "</td>" +
                    "<td class='bordercell'>" +
                        "{usuario}" +
                    "</td>" +
                    "<td class='bordercell'>" +
                        "{tipousuario}" +
                    "</td>" +
                    "<td class='bordercell'>" +
                        "{fecha}" +
                    "</td>" +
                    "<td class='bordercell'>" +
                        "{destacado} <a href='/post/highlight?id=" + post.id.toString() + "'>Cambiar Destacado</a>" +
                    "</td>" +
                    "<td class='bordercell'>" +
                        "{estatus} <br/> <a href='/post/status?id=" + post.id.toString() + "'>Cambiar Estatus</a>" +
                    "</td>" +
                "</tr>"
    return htmlRet.replace("{titulo}",post.titulo).replace("{contenido}",post.contenido).replace("{usuario}",post.alias)
                  .replace("{tipousuario}",post.tipo).replace("{fecha}",post.fecha).replace("{estatus}",post.estatus)
                  .replace("{destacado}",post.destacado)
}

function getImageOptions(file){
    return "<option value='img/" + file + "' >" + file + "</option>";
}

function isPair(number){
    var i = parseInt(number/2);
    var d = number/2;
    if (d-i == 0){
        return true;
    }
    else{
        return false;
    }
}