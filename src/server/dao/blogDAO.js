const mysql = require("mysql");
const config = require("../../config.json"); 

const dbConfig = {
    host     : config.mySQL.host,
    user     : config.mySQL.user,
    password : config.mySQL.password,
    database : config.mySQL.database};

function handleDisconnect() {
    connection = mysql.createConnection(dbConfig); // Recreate the connection, since
                                                    // the old one cannot be reused.
    connection.connect(function(err) {              // The server is either down
        if(err) {                                     // or restarting (takes a while sometimes).
            console.log('error when connecting to db:', err);
            setTimeout(handleDisconnect, 2000); // We introduce a delay before attempting to reconnect,
        }                                     // to avoid a hot loop, and to allow our node script to
    });                                     // process asynchronous requests in the meantime.
                                            // If you're also serving http, display a 503 error.
    connection.on('error', function(err) {
        console.log('db error', err);
        if(err.code === 'PROTOCOL_CONNECTION_LOST') { // Connection to the MySQL server is usually
            handleDisconnect();                         // lost due to either server restart, or a
        } 
        else{                                      // connnection idle timeout (the wait_timeout
            throw err;                                  // server variable configures this)
        }
    });
}
handleDisconnect();

module.exports = {
    InsertUser : InsertUser,
    LoginUser : LoginUser,
    getPosts : getPosts,
    getPostDetail : getPostDetail,
    InsertComment : InsertComment,
    getPostComments : getPostComments,
    getPostsByIdUser : getPostsByIdUser,
    getUsersList : getUsersList,
    updateUserStatus : updateUserStatus,
    updatePostStatus : updatePostStatus,
    updatePostHighlight : updatePostHighlight,
    insertPost : insertPost,
    insertPostDetail : insertPostDetail,
    insertLog : insertLog
}


function InsertUser(body, callback){
    
    if(body.facebook == undefined || body.facebook == null){
        body.facebook = "";
    }
    if(body.twitter == undefined || body.twitter == null){
        body.twitter = "";
    }
    if(body.instagram == undefined || body.instagram == null){
        body.instagram = "";
    }
    if(body.youtube == undefined || body.youtube == null){
        body.youtube = "";
    }

    connection.query("CALL InsertaUsuario('" + body.nombre + "','" 
                                             + body.apellidos + "','"
                                             + body.correo + "','"
                                             + body.contrasenia + "','"
                                             + body.celular + "','"
                                             + body.alias + "',"
                                             + body.tipo.toString() + ",'"
                                             + body.facebook + "','"
                                             + body.twitter + "','"
                                             + body.instagram + "','"
                                             + body.youtube +"')",function(error, result, fields){
        if(error){
            callback(error);
            return;
        }
        
        callback(null,result);
        return;
    });
}

function LoginUser(body, callback){
    connection.query("CALL LoginUsuario('" + body.correo + "','" + body.contrasenia + "')",function(error,result,fields){
        if(error){
            callback(error);
            return;
        }
        callback(null,result);
        return;
    })
}

function getPosts(pinnedUp,callback){
    connection.query("CALL ObtenerPublicaciones("+ pinnedUp.toString() +")",function(error,result,fields){
        if(error){
            callback(error);
            return;
        }
        callback(null,result);
        return;
    })
}

function getPostDetail(id,callback){
    connection.query("CALL ObtenerPublicacion("+ id.toString() +")",function(error,result,fields){
        if(error){
            callback(error);
            return;
        }
        callback(null,result);
        return;
    })
}

function InsertComment(idpost, iduser, comment, callback){
    connection.query("CALL InsertaComentario(" + idpost.toString() + "," + iduser.toString() + ",'" + comment + "')",function(error,result,fields){
        if(error){
            callback(error);
            return;
        }
        callback(null,result);
        return;
    });   
}

function getPostComments(id,callback){
    connection.query("CALL ComentariosPorPublicacion(" + id.toString() + ")",function(error,result,fields){
        if(error){
            callback(error);
            return;
        }
        callback(null,result);
        return;
    });
}

function getPostsByIdUser(id,callback){
    connection.query("CALL ObtenerPublicacionesPorUsuario(" + id.toString() + ")",function(error,result,fields){
        if(error){
            callback(error);
            return;
        }
        callback(null,result);
        return;
    })
}

function getUsersList(callback){
    connection.query("CALL ObtenerListadoDeUsuarios()",function(error,result,fields){
        if(error){
            callback(error);
            return;
        }
        callback(null,result);
        return;
    });
}

function updateUserStatus(iduser,callback){
    connection.query("CALL CambiarEstatusUsuario(" + iduser.toString() + ")",function(error,result,fields){
        if(error){
            callback(error);
            return;
        }
        callback(null,result);
        return;
    });
}

function updatePostStatus(idpost, callback){
    connection.query("CALL CambiarEstatusPublicacion(" + idpost.toString() + ")",function(error,result,fields){
        if(error){
            callback(error);
            return;
        }
        callback(null,result);
        return;
    });
}

function updatePostHighlight(idpost, callback){
    connection.query("CALL CambiarPublicacionDestacada(" + idpost.toString() + ")",function(error,result,fields){
        if(error){
            callback(error);
            return;
        }
        callback(null,result);
        return;
    });
}

function insertPost(title,content,iduser,imgbanner,callback){
    connection.query("CALL InsertaPublicacion('" + title + "','" + content + "'," + iduser.toString() + ",'" + imgbanner + "')",function(error,result,fields){
        if(error){
            callback(error);
            return;
        }
        callback(null,result);
        return;
    });
}

function insertPostDetail(idpost,header,detail,callback){
    connection.query("CALL AgregaSeccion(" + idpost.toString() + ",'" + header + "','" + detail + "')",function(error,result,fields){
        if(error){
            callback(error);
            return;
        }
        callback(null,result);
        return;
    });
}

function insertLog(iduser,idpost,action){
    connection.query("CALL InsertaBitacora(" + iduser.toString() + "," + idpost.toString() + ",'" + action + "')",function(error,result,fields){
        if(error){console.log(error);}
    });
}