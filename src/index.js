const oUser = require("./server/controllers/user.js");
const oPost = require("./server/controllers/post.js");
const oComment = require("./server/controllers/comment.js");
const oSession = require("./server/controllers/session.js");
const config = require("./config.json");
const express = require("express");
const session = require("express-session");
const myParser = require("body-parser");
const path = require("path");
var app = express();
var router = express.Router();
const multer = require("multer");
const storage = multer.diskStorage({
    destination : (req,file,callback) =>{
        callback( null,'./client/imgs')
    },
    filename : (req,file,callback) =>{
        console.log(file);
        callback(null,file.originalname);
    }
});
const upload = multer({storage});

//User
router.route("/user").post(oUser.addUser);
router.route("/user/admin").get(oUser.getUsersAdmin);
router.route("/user/login").post(oUser.loginUser);
router.route("/user/add").get(oUser.redirectAddUser);
router.route("/user/admin/add").post(oUser.addUserByAdmin);
router.route("/user/status").get(oUser.userChangeStatus);

//Post
router.route("/posts").get(oPost.getPosts);
router.route("/").get(oPost.getLanding);
router.route("/post/detail").get(oPost.getPost);
router.route("/post/admin").get(oPost.getAdminPosts);
router.route("/post/status").get(oPost.updateStatus);
router.route("/post/highlight").get(oPost.updateHighlight);
router.route("/post/new").get(oPost.redirectNewPost);
router.route("/post/add").post(oPost.addNewPost);
router.route("/uploadtest").get(oPost.redirectUploadTest);
router.route("/upload").post(upload.single("imgbanner"),oPost.postUpload);

//Session
router.route("/login").get(oSession.getLoginRedirect);
router.route("/logout").get(oSession.logout);

//Comment
router.route("/comment").post(oComment.InsertComment);

app.use(myParser.json());
app.use(session({secret:config.secret})); // para manejar sesiones
app.use(express.urlencoded({extended : false})); // para tomar los valores de los post
app.use('/', router);
app.use('/imgs', express.static(path.join(__dirname, './client/imgs')));
app.use('/img', express.static(path.join(__dirname, './client/imgs')));
app.use('/css', express.static(path.join(__dirname, './client/css')));

//Puerto asignado por el server, si no, lo que hay en config
app.set('port', process.env.PORT || config.port)

app.listen(app.get("port"),function(){console.log("Started on PORT ", app.get("port"));});


